import sys,time
import SimpleHTTPServer
import SocketServer

PORT = int(sys.argv[1])

DATA = [1,4,3,4,5,1,2,4,3,1,5]

class MyHandler(SocketServer.BaseRequestHandler):

  def handle(self):
    global DATA
    self.request.settimeout(1)
    d=""
    i=0
    while True:
        i+=1
        time.sleep(0.1)
        if i==10: break
        try:
            data = self.request.recv(1024)
        except Exception,ex:
            print ex
            break
        d+=data
        #if d.find("\r\n\r\n")!=-1: break
        if d.find("\n")!=-1: break
        if len(d)>10:break
    #sp=d.replace("\r\n","\n").split("\n")
    sp=d.split("\n")
    ind=0
    for l in sp:
        if l.find("GET ")==0:
            a=l.split(" ")
            try:ind=int(a[1][1:])
            except: ind=0
    try:val=DATA[ind]
    except: val=5
    t='{"id":%d,"rating":%d}' % (ind, val)
#    t=repr(d)
    response = """HTTP/1.0 200 OK
Content-Type: text/html
Content-Length: %d

%s
""" % (len(t), t)
    self.request.sendall(response)
    self.request.close()


SocketServer.TCPServer.allow_reuse_address = True
httpd = SocketServer.TCPServer(("", PORT), MyHandler)

print "serving at port", PORT
httpd.serve_forever()

